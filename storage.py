""" Module to handle simple locale dict Storage """
import pickle

# place to save the storagefile
filename = 'storage.p'

# this is the storagedict to save all key-value-pairs
_storage = dict()

def read():
    """ Reads the storage from file """
    return pickle.load(open(filename, "rb"))

def save():
    """ Saves the storage to file """
    pickle.dump(_storage, open(filename, "wb"))

def init():
    """ Initalize the storage - reads it or keep empty """
    global _storage
    try:
        _storage = read()
    except IOError:
        _storage = dict()

def update(**kwargs):
    """
    update/add values to storage. Accepts dict or tuples
    example: update(key=value, key2=value2)
             update({'key': value})
    """
    global _storage
    _storage.update(kwargs)
    save()

def get(key):
    """ returns a value from given key """
    return _storage.get(key)

def list():
    """ returns the complete storage """
    return _storage

def delete(key):
    """ delets key with value from storage """
    _storage.pop(key)
    save()

def drop():
    """
    Drops all key-value-pairs from storage - also in file.
    Be careful cause there is no backup!
    """
    global _storage
    _storage = dict()
    save()

if __name__ == '__main__':
    init()
    # update(pi=3.1415, answertoanything=42, test='this is a simple test')
    # test = get('pi')
    # print(test, type(test))
    # delete('pi')
    # drop()
    print(_storage)
